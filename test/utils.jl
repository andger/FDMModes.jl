n = [1, 2, 3, 2, 1]
dx = 1
wavelength = 1
@test FDMModes.estimate_num_modes(dx, n, wavelength) == 18
wavelength = 2
@test FDMModes.estimate_num_modes(dx, n, wavelength) == 9
dx = [3, 2, 1, 2, 3]
wavelength = 1
@test FDMModes.estimate_num_modes(dx, n, wavelength) == 34

n = ones(3, 4)
n[1, 2] = 2
dx = 1
dy = 1
wavelength = 1
@test FDMModes.estimate_num_modes(dx, dy, n, wavelength) == 80
wavelength = 2
@test FDMModes.estimate_num_modes(dx, dy, n, wavelength) == 20
dx = [1, 2, 3]
dy = [4, 3, 2, 1]
wavelength = 1
@test FDMModes.estimate_num_modes(dx, dy, n, wavelength) == 364
