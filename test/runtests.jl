import FDMModes
import Test: @test_broken, @test, @test_throws

tests = ["matrix_math", "laplacian", "modesolver", "utils"]

approxeq(a, b; rtol = 1e-4) = all(isapprox.(a, b, rtol = rtol))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
