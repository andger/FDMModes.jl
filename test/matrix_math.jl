#=
Test whether matrix operations involving sparse/special matrices conserve
sparseness/special-type. As non-sparse matrices should be avoided in
calculations, and special types of matrices may have faster eigensolver
and other math routines.
=#
import SparseArrays, LinearAlgebra

N = 10
centerd = Float64.(1:N)
lowerd = centerd[2:end]
stdmat = LinearAlgebra.SymTridiagonal(centerd, lowerd)
stdmatsparse = SparseArrays.sparse(stdmat)
tdmat = LinearAlgebra.Tridiagonal(lowerd, centerd, lowerd)
tdmatsparse = SparseArrays.sparse(tdmat)
dmat = LinearAlgebra.Diagonal(centerd)
dmatsparse = SparseArrays.sparse(dmat)

@test typeof(stdmat + stdmat) == LinearAlgebra.SymTridiagonal{Float64,Array{Float64,1}}
@test typeof(tdmat + stdmat) == LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test typeof(dmat + stdmat) == LinearAlgebra.SymTridiagonal{Float64,Array{Float64,1}}
@test typeof(tdmat + tdmat) == LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test typeof(dmat + tdmat) == LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test typeof(dmat + dmat) == LinearAlgebra.Diagonal{Float64,Array{Float64,1}}
@test typeof(stdmat + dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(tdmat + dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(dmat + dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}

@test typeof(stdmat * stdmat) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(tdmat * stdmat) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(dmat * stdmat) == LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test typeof(tdmat * tdmat) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(dmat * tdmat) == LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test typeof(dmat * dmat) == LinearAlgebra.Diagonal{Float64,Array{Float64,1}}
@test typeof(stdmat * dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(tdmat * dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(dmat * dmatsparse) == SparseArrays.SparseMatrixCSC{Float64,Int64}

@test_broken typeof(kron(stdmat, stdmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(tdmat, stdmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(stdmat, tdmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(dmat, stdmat)) ==
             LinearAlgebra.SymTridiagonal{Float64,Array{Float64,1}}
@test_broken typeof(kron(stdmat, dmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(tdmat, tdmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(dmat, tdmat)) ==
             LinearAlgebra.Tridiagonal{Float64,Array{Float64,1}}
@test_broken typeof(kron(tdmat, dmat)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(kron(dmat, dmat)) == LinearAlgebra.Diagonal{Float64,Array{Float64,1}}
@test_broken typeof(kron(stdmat, dmatsparse)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test_broken typeof(kron(tdmat, dmatsparse)) == SparseArrays.SparseMatrixCSC{Float64,Int64}
@test typeof(kron(dmat, dmatsparse)) == SparseArrays.SparseMatrixCSC{Float64,Int64}

# Test whether there are optimized LinearAlgebra.eigen routines available
# for these matrix types
import LinearAlgebra
@test_broken LinearAlgebra.eigen(LinearAlgebra.Tridiagonal(ones(4), -2 * ones(5), ones(4)))
