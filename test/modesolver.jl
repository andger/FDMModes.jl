# Compare results to those given in Tables A17.1 and A17.2 of Coldren book
# Page 696

wavelength = 1.3
function nfun(d, dx)
    x = dx:dx:(1.5+d)
    [x < 1 ? 3.1 : (x < 1 + d ? 3.4 : 1) for x in x]
end

# Single transverse mode with neff value of 3.279790
d = 0.5
dx = 0.25
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 1)
@test approxeq(neffs[1], 3.31523)
dx = 0.125
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 1)
@test approxeq(neffs[1], 3.29213)
dx = 0.0625
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 1)
@test approxeq(neffs[1], 3.28329)
dx = 0.03125
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 1)
@test approxeq(neffs[1], 3.28070)

# Two transverse modes with neff values of 3.357718 and 3.232331
d = 1
dx = 0.25
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 2)
@test approxeq(neffs[1], 3.36583)
@test approxeq(neffs[2], 3.27223)
dx = 0.125
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 2)
@test approxeq(neffs[1], 3.36046)
@test approxeq(neffs[2], 3.24507)
dx = 0.0625
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 2)
@test approxeq(neffs[1], 3.35849)
@test approxeq(neffs[2], 3.23582)
dx = 0.03125
index = nfun(d, dx)
neffs, _ = FDMModes.waveguidemodes(index, wavelength, dx, nev = 2)
@test approxeq(neffs[1], 3.35792)
@test approxeq(neffs[2], 3.23322)

# Simple test comparing to values obtained from original WGMODES code as
# run on Octave
lambda = 1.0
dx = dy = 0.01
n = ones(200, 300)
n[50:100, 100:200] *= 3
neff, _ = FDMModes.waveguidemodes(n, lambda, dx, dy, nev = 6)
@test approxeq(neff, [2.8568, 2.7515, 2.5680, 2.5069, 2.3872, 2.2909])

# Simple test comparing to values obtained from original WGMODES code as
# run on Octave
wavelength = 2.0
dx = ones(200) ./ 100
dx[1:5:200] .*= 2
dy = ones(300) ./ 100
dy[1:10:300] .*= 1.5
n = ones(201, 301)
n[1:99, 1:301] .= 1.5
n[100:110, 1:301] .= 3
n[100:150, 100:201] .= 3
neff, _ = FDMModes.waveguidemodes(n, wavelength, dx, dy, nev = 4)
@test approxeq(neff, [2.6583, 2.3354, 1.8898, 1.7701])

wavelength = 2.0
dx = ones(200) ./ 100
dx[1:5:200] .*= 2
dy = ones(300) ./ 100
dy[1:10:300] .*= 1.5
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
neff, _ = FDMModes.waveguidemodes(n, wavelength, dx, dy, nev = 4)
@test approxeq(neff, [2.6583, 2.3304, 1.8943, 1.7664])

# Compare uniform to non-uniform sampling
wavelength = 1
nfun(x) = @. 1 + exp(-x^2)
dxs = 0.05
xs = -4:dxs:4
dxv = reduce(vcat, fill([0.05, 0.025, 0.025], round(Int, 8 / 0.1)))
xv = [0; cumsum(dxv)] .- 4
ns, nv = nfun(xs), nfun(xv)
neffs, _ = FDMModes.waveguidemodes(ns, wavelength, dxs, nev = 4)
neffv, _ = FDMModes.waveguidemodes(nv, wavelength, dxv, nev = 4)
@test approxeq(neffs[1:4], neffv, rtol = 1e-3)

wavelength = 1
nfun(x, y) = 1 + exp(-x^2 - y^2)
dxs = 0.05
xs = -4:dxs:4
dys = 0.04
ys = -4:dys:4
dxv = reduce(vcat, fill([0.05, 0.025, 0.025], round(Int, 8 / 0.1)))
dyv = reduce(vcat, fill([0.04, 0.02, 0.02], round(Int, 8 / 0.08)))
xv = [0; cumsum(dxv)] .- 4
yv = [0; cumsum(dyv)] .- 4
ns, nv = [nfun(x, y) for x in xs, y in ys], [nfun(x, y) for x in xv, y in yv]
neffs, _ = FDMModes.waveguidemodes(ns, wavelength, dxs, dys, nev = 4)
neffv, _ = FDMModes.waveguidemodes(nv, wavelength, dxv, dyv, nev = 4)
@test approxeq(neffs[1:4], neffv, rtol = 1e-3)
