dx = 0.5
dy = 0.25
x = [0:dx:10...]
y = [0:dy:11...]
Nx = length(x)
Ny = length(y)

# Test equivalence between uniform and non-uniform Laplacians
@test approxeq(FDMModes.laplacian_x(x), FDMModes.laplacian_dx(dx, Nx))
@test approxeq(FDMModes.laplacian_x(x, y), FDMModes.laplacian_dx(dx, Nx, dy, Ny))

dx = 1:10
dy = 1:11
x = [0; cumsum(dx)]
y = [0; cumsum(dy)]

# Test equivalence between `x` and `dx` forms of non-uniform Laplacians
@test approxeq(FDMModes.laplacian_x(x), FDMModes.laplacian_dx(dx))
@test approxeq(FDMModes.laplacian_x(x, y), FDMModes.laplacian_dx(dx, dy))
