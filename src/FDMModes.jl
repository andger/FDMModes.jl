module FDMModes

include("utils.jl")
include("laplacian.jl")
include("modesolver.jl")

export waveguidemodes

end
