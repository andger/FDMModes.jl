import LinearAlgebra, Arpack, SparseArrays

#TODO: Adapt eigensolve for antiguided systems?
"""
    eigensolve(A, min_val, max_val; nev = 6, maxiter = 1000) -> (eigenvalues, eigenvectors)

Solve for the eigenmodes of matrix `A`. It will try to choose the sanest
eigensolver and parameters based on the inputs.

# Arguments
- `A::AbstractMatrix`: the finite difference problem in matrix form.
- `min_val::Number`: lower bound for the eigenvalues.
- `max_val::Number`: upper bound for the eigenvalues.
- `nev::Integer = 6`: maximal number of eigensolutions to solve for (used by iterative eigensolver only).
- `maxiter::Integer = 1000`: maximum number of iterations executed (used by iterative eigensolver only).

If you get `Arpack` errors, then the number of iterations likely needs
to be increased using the `maxiter` argument.

If not all of the desired (bound) modes are found, then `nev` may need
to be increased.

In terms of computational efficiency, `A` is best given in the most
specific form of a matrix, with `SymTridiagonal` > `Tridiagonal` >
`AbstractMatrix`. This enables better eigensolver algorithms and more
targeted eigensolution calculation.
"""
function eigensolve(
    A::LinearAlgebra.SymTridiagonal,
    min_val::Number,
    max_val::Number;
    nev::Integer = 6,
    maxiter::Integer = 1000,
)
    sol = LinearAlgebra.eigen(A, min_val, max_val)
    sol.values, sol.vectors
end
function eigensolve(
    A::LinearAlgebra.Tridiagonal,
    min_val::Number,
    max_val::Number;
    nev::Integer = 6,
    maxiter::Integer = 1000,
)
    sol = Arpack.eigs(A, sigma = max_val, nev = nev, maxiter = maxiter)
    idx = findall(x -> min_val <= real(x) <= max_val, sol[1])
    sol[1][idx], sol[2][:, idx]
end
function eigensolve(
    A::AbstractMatrix,
    min_val::Number,
    max_val::Number;
    nev::Integer = 6,
    maxiter::Integer = 1000,
)
    sol = Arpack.eigs(A, sigma = max_val, nev = nev, maxiter = maxiter)
    idx = findall(x -> min_val <= real(x) <= max_val, sol[1])
    sol[1][idx], sol[2][:, idx]
end

"""
    modesolver(A, Dims, min_index, max_index; nev = 6, maxiter = 1000) -> (neffs, modes)

Find the modal effective indices and modal fields of the eigenmatrix `A`.

# Arguments
- `A::AbstractMatrix`: the finite difference problem in matrix form.
- `Dims::Tuple`: dimensions of the problem finite grid (used to reshape the modal fields to the correct size).
- `min_index::Number`: lower bound for the modal effective index.
- `max_index::Number`: upper bound for the modal effective index.
- `nev::Integer = 6`: maximal number of modes to solve for (used by iterative eigensolver only).
- `maxiter::Integer = 1000`: maximum number of iterations executed (used by iterative eigensolver only).

`modesolver` will try to find the modes with (real components of the) modal effective
indices between `min_index` and `max_index`. If an iterative solver is
used, then up to `nev` modes will be found. `modes` will be an array whose
last index is the mode number, and the others are defined by the dimensions
of the problem grid `Dims`.

If you get `Arpack` errors, then the number of iterations likely needs
to be increased using the `maxiter` argument.

If not all of the desired (bound) modes are found, then `nev` may need
to be increased.

In terms of computational efficiency, `A` is best given in the most
specific form of a matrix, with `SymTridiagonal` > `Tridiagonal` >
`AbstractMatrix`. This enables better eigensolver algorithms and more
targeted eigensolution calculation.
"""
function modesolver(
    A::AbstractMatrix,
    Dims::Tuple,
    min_index::Number,
    max_index::Number;
    nev::Integer = 6,
    maxiter::Integer = 1000,
)
    eigenvalues, eigenvectors = eigensolve(A, min_index^2, max_index^2, nev = nev)
    neffs = sqrt.(eigenvalues)
    idx = sortperm(real.(neffs), rev = true)
    neffs, modes = neffs[idx], reshape(eigenvectors[:, idx], Dims..., :)
    neffs, modes
end

"""
    waveguidemodes(index, wavelength, dx[, dy; nev = nothing, maxiter = 1000]) -> (neffs, modes)

Solve for the eigenmodes and modal effective indices of a dielectric
waveguide of index structure `index` at wavelength of `wavelength`.

# Arguments
- `index::AbstractVecOrMat`: the refractive index structure of the waveguide problem.
- `wavelength::Number`: free-space wavelength.
- `dx::Union{Number, AbstractVector}`: grid spacing for the index (either uniform or non-uniform).
- `dy::Union{Number, AbstractVector}`: grid spacing for the index (either uniform or non-uniform).
- `nev = 6`: maximal number of modes to solve for (used by iterative eigensolver only). If `nev = nothing` then an estimated upper bound on the number of modes will be calculated (usually a very large number).
- `maxiter::Integer = 1000`: maximum number of iterations executed (used by iterative eigensolver only).

`waveguidemodes` will try to find the modes of the waveguide structure
`index` with uniform or non-uniform sampling `dx` (and `dy` for 2D). It
will try to find all the modes that could be confined (modes whose modal
effective index is bound by the extrema values of real components of
`index`. By default up to 6 modes will be found by the iterative
eigensolver. If `nev = nothing` it may solve for a large amount
of potential eigensolutions to determine all possible eigenmodes, but
the method by which it chooses the number of modes to search for is
somewhat primitive and will likely lead to solving for way too many
eigensolutions, many of which will be spurious results (especially given
a relatively large waveguide structure). In order to avoid running solving
for too long, it may be wise to provide an estimated number of modes
by setting `nev` to a reasonable value. If only a few modes
are desired, providing a value for `nev` would be wise to
avoid calculating spurious or undesired mode solutions. 

If you get `Arpack` errors, then the number of iterations likely needs
to be increased using the `maxiter` argument.

If not all of the desired (bound) modes are found, then `nev` may need
to be increased.

In terms of computational efficiency, 1D problems with uniform grid spacing
(scalar `dx`) are the best. Non-uniform grid spacing will lead to less
efficient eigensolver algorithms being used, but in choosing between using
uniform and non-uniform sampling in 1D one must consider the trade-off
between faster algorithms solving a larger problem (uniform sampling)
and slower algorithms solving a smaller problem (non-uniform sampling).
In 2D, there is (likely) no performance benefit from uniform sampling.
"""
function waveguidemodes(
    index::AbstractVector,
    wavelength::Number,
    dx::Number;
    nev = 6,
    maxiter::Integer = 1000,
)
    nev = isnothing(nev) ? estimate_num_modes(dx, index, wavelength) : nev
    k = 2 * pi / wavelength
    dX = dx * k
    L = laplacian_dx(dX, length(index))
    A = L + LinearAlgebra.Diagonal(index .^ 2)
    modesolver(A, size(index), minimum(real.(index)), maximum(real.(index)), nev = nev)
end
function waveguidemodes(
    index::AbstractVector,
    wavelength::Number,
    dx::AbstractVector;
    nev = 6,
    maxiter::Integer = 1000,
)
    @assert length(index) == length(dx) + 1 || length(index) == length(dx)
    if length(index) == length(dx)
        dx = @. (dx[1:end-1] + dx[2:end]) / 2
    end
    nev = isnothing(nev) ? estimate_num_modes(dx, index, wavelength) : nev
    k = 2 * pi / wavelength
    dX = dx .* k
    L = laplacian_dx(dX)
    A = L + LinearAlgebra.Diagonal(index .^ 2)
    modesolver(A, size(index), minimum(real.(index)), maximum(real.(index)), nev = nev)
end
function waveguidemodes(
    index::AbstractMatrix,
    wavelength::Number,
    dx::Number,
    dy::Number;
    nev = 6,
    maxiter::Integer = 1000,
)
    nev = isnothing(nev) ? estimate_num_modes(dx, dy, index, wavelength) : nev
    k = 2 * pi / wavelength
    dX = dx * k
    dY = dy * k
    L = laplacian_dx(dX, size(index)[1], dY, size(index)[2])
    A = L + LinearAlgebra.Diagonal(index[:] .^ 2)
    modesolver(A, size(index), minimum(real.(index)), maximum(real.(index)), nev = nev)
end
function waveguidemodes(
    index::AbstractMatrix,
    wavelength::Number,
    dx::AbstractVector,
    dy::AbstractVector;
    nev = 6,
    maxiter::Integer = 1000,
)
    @assert size(index, 1) == length(dx) + 1 || size(index, 1) == length(dx)
    @assert size(index, 2) == length(dy) + 1 || size(index, 2) == length(dy)
    if size(index, 1) == length(dx)
        dx = @. (dx[1:end-1] + dx[2:end]) / 2
    end
    if size(index, 2) == length(dy)
        dy = @. (dy[1:end-1] + dy[2:end]) / 2
    end
    nev = isnothing(nev) ? estimate_num_modes(dx, dy, index, wavelength) : nev
    k = 2 * pi / wavelength
    dX = dx .* k
    dY = dy .* k
    L = laplacian_dx(dX, dY)
    A = L + LinearAlgebra.Diagonal(index[:] .^ 2)
    modesolver(A, size(index), minimum(real.(index)), maximum(real.(index)), nev = nev)
end
