using Documenter
import FDMModes

makedocs(
    sitename = "FDMModes.jl",
    pages = [
        "Home" => "index.md",
        "Examples" => [
            "1D Slab Waveguide" => "example_1d_wvg.md",
            "2D Circular Core Waveguide" => "example_2d_wvg.md",
        ],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "laplacian.jl" => "laplacian.md",
            "modesolver.jl" => "modesolver.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "laplacian.jl" => "laplacian_test.md",
            "matrix_math.jl" => "matrix_math_test.md",
            "modesolver.jl" => "modesolver_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
