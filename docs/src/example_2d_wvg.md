# Example: 2D Circular Core Waveguide Modes

Let's calculate the modes of a circular core 2D waveguide, similar
to a simple optical fiber. We'll assume
a core index value of 1.46 and cladding index value of 1.45, waveguide
core diameter of 8 $\mu$m and wavelength of 1.55 $\mu$m. First
we'll import the package and define the relevant variables:

```@example 2d
import FDMModes

lambda=1.55
n_core, n_clad=1.46, 1.45
waveguide_width=8
dx=dy=0.1
x=y=-10:dx:10
index=[sqrt(x^2+y^2)<=waveguide_width/2 ? n_core : n_clad for x=x, y=y]
nothing # hide
```

We plot the index structure using the `Plots` package:

```@example 2d
import Plots: plot, scatter, heatmap
import Plots: savefig # hide
heatmap(x, y, index,
	xlabel="x",
	ylabel="y",
	aspectratio=1,
	legend=true,
	size=(800, 800),
)
savefig("fiber_wvg_index.png"); nothing # hide
```

![Plot of the refractive index for a 2D circular core waveguide](fiber_wvg_index.png)

Now that we've made the waveguide index structure we can calculate the
waveguide eigenmodes. We will only look for up to 4 waveguide modes,
by setting `nev=4`. If we don't set `nev`, the mode solver will calculate
a lot more eigensolutions than there are actual modes, so we give it a guess
for the number of modes:

```@example 2d
neffs, modes=FDMModes.waveguidemodes(index, lambda, dx, dy, nev=4)
nothing # hide
```

We now plot the results:

```@example 2d
plot(
	scatter(neffs, ylabel="Modal Effective Index", legend=false),
	heatmap(x, y, modes[:, :, 1],
		xlabel="x",
		ylabel="y",
		aspectratio=1,
	),
	heatmap(x, y, modes[:, :, 2],
		xlabel="x",
		ylabel="y",
		aspectratio=1,
	),
	heatmap(x, y, modes[:, :, 3],
		xlabel="x",
		ylabel="y",
		aspectratio=1,
	),
	size=(800, 800),
)
savefig("fiber_wvg_sol.png"); nothing # hide
```

![Plot of the waveguide eigenmode solutions](fiber_wvg_sol.png)

The mode-solver has found 3 waveguide modes with mode profiles that
qualitatively look as expected. We have a fundamental mode and a pair
of degenerate higher order modes whose field profiles are similar
after a $\frac{\pi}{2}$ rotation.
