# FDMModes

## About

`FDMModes.jl` is a Julia package for solving for
the eigenmodes of dielectric waveguides using the finite difference
method (FDM) in the frequency domain. It also provides various
utilities that are useful when analyzing or designing dielectric
waveguides or lasers based on dielectric waveguides.

## Features

* Solve for the scalar modal fields
* Support for waveguide structures defined by isotropic refractive
index structures
* Support for index structure defined on a finite, uniform or non-uniform,
discrete Cartesian grid
* Utilities for direct design of waveguides for a given mode profile (upcoming)

Should you require support for anisotropic materials or (full- or semi-)
vectorial fields, please try the MatLab code
[WGMODES](https://photonics.umd.edu/software/wgmodes/)
or the Julia port
[WGMODES.jl](https://gitlab.com/pawelstrzebonski/WGMODES.jl).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMModes.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for FDMModes.jl](https://pawelstrzebonski.gitlab.io/FDMModes.jl/).

## References

The basic finite difference method for solving the scalar modes of a
dielectric waveguide on a uniform grid is based on the simple algorithm
described in Appendix 17 of
[Larry A. and Scott Corzine. "Diode Lasers and Photonic Integrated Circuits." (1995)](https://doi.org/10.1117/1.601191).
Support for non-uniform grids was derived to improve the method in
the book.

The various utilities for waveguide and mode engineering, including
direct design of waveguide index structure from modal field can be found
in my master's thesis,
["Semiconductor Laser Mode Engineering via Waveguide Index Structuring"](http://hdl.handle.net/2142/102511).
